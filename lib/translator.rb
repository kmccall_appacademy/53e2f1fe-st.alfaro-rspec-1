require 'byebug'
module Translator
  VOWELS = [:a, :e, :i, :o, :u]
  SPECIAL_CONSONANT = [:qu]
  ALPHABET = (:a..:z).to_a

  def self.translate_word(word)
    process_word(word)
  end

  private_class_method def self.process_word(word)
    punctuation_index = punctuation_index(word.split(""))
    punctuation = String.new
    if punctuation_index != -1
      punctuation = word[punctuation_index...word.length]
      word = word[0...punctuation_index]
    end

    word_chars = word.split("")
    translate_word = String.new

    if VOWELS.include?(word_chars[0].downcase.to_sym)
      translate_word = process_vowel_first(word)
    else
      translate_word = process_consonant_first(word)
    end
    translate_word + punctuation
  end

  private_class_method def self.first_consonants(word_chars)
    prev = String.new
    word_chars.each_with_index do |element, index|
      special_consonant = prev.concat(element).downcase
      if !is_special_consonant?(special_consonant) && is_vowel?(element)
        return index
      end
      prev = element
    end
  end

  private_class_method def self.process_vowel_first(word)
    "#{word}ay"
  end

  private_class_method def self.process_consonant_first(word)
    word_chars = word.split("")
    first_consonant = first_consonants(word_chars)
    prefix = word[first_consonant...word.length].to_s
    sufix = "#{word[0...first_consonant]}ay"
    prefix + sufix
  end

  private_class_method def self.punctuation_index(word_chars)
    found_index = -1
    word_chars.each_with_index do |element, index|
      if !ALPHABET.include?(element.downcase.to_sym)
        found_index = index
        break
      end
    end
    found_index
  end

  private_class_method def self.is_special_consonant?(word)
    SPECIAL_CONSONANT.include?(word.to_sym)
  end

  private_class_method def self.is_vowel?(char)
    VOWELS.include?(char.downcase.to_sym)
  end

end
