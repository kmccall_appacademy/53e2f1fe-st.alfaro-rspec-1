F_C_PROPORTION = 5.to_f/9 # or use 5.0/9 to force floating-point
F_C_FREEZE_DELTA = 32

def ftoc f_temp
  (f_temp - F_C_FREEZE_DELTA) * F_C_PROPORTION
end

def ctof c_temp
  (c_temp * 1 / F_C_PROPORTION) + F_C_FREEZE_DELTA
end
