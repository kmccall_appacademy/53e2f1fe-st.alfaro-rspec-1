require 'translator'

def translate(sentence)
  words = sentence.split(" ")
  words.map! { |word| Translator.translate_word(word) }
  words.join(" ")
end
