LITTLE_WORDS = [:and, :the, :over]

def echo(word)
  word
end

def shout(word)
  word.upcase
end

def repeat(word, repeat_n = 2)
  #debugger
  repeated = String.new()
  repeat_n.times { repeated.concat"#{word} " }
  repeated.strip
end


def start_of_word(word, index)
  word[0...index]
end

def first_word(sentence)
  sentence.split(" ")[0]
end

def titleize(sentence)
  words = sentence.split(" ")

  words.map!.with_index do |element, index|
    capitalize?(element, index) ? element.capitalize! : element
  end
  words.join(" ")
end

def capitalize?(element, index)
  index == 0 || !LITTLE_WORDS.include?(element.to_sym)
end
