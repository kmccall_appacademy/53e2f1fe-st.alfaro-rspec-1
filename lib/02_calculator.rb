def add(x, y)
  x + y
end

def subtract(x, y)
  x - y
end

def sum parameters
  parameters.reduce(0, :+)
end

def multiply(*params)
  !params.empty? ? params.reduce(1, :*) : 0
end

def power(base, power = 0)
  base ** power
end

def factorial(n = 0)
  ##For math reasons 0! = 1
  n == 0 ? 1 : multiply(*(1..n).to_a)
end
